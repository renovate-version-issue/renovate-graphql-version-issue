plugins {
    kotlin("jvm") version "1.8.10"
}

allprojects {
    repositories {
        mavenCentral()
    }
}

dependencies {
    implementation("com.graphql-java:graphql-java-extended-scalars:2023-01-24T02-11-56-babda5f")
}