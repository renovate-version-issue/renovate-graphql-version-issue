Renovate Version Issue
======================

This is a minimal repo to recreate a potential issue using the [Renovate](https://github.com/renovatebot/renovate) library, when it attempts to upgrade the [graphql-java-extended-scalars](https://github.com/graphql-java/graphql-java-extended-scalars) library.


This repo contains minimal gradle (kotlin) build setup, with the single `graphql-java-extended-scalars` dependency in the `build.gradle.kts` file.

Running renovate on this dependency should upgrade the current version `19.1` to the latest stable version `20.0`, however it upgrades to what appears to be a SNAPSHOT version - `2023-01-24T02-11-56-babda5f`


## Links  

A discussion has been opened up in the [graphql-java](https://github.com/graphql-java/graphql-java/discussions/3106) library discussion board to confirm versioning patterns.  

Maven Central repository [link](https://mvnrepository.com/artifact/com.graphql-java/graphql-java-extended-scalars) for this library.  

